<?php

function getFullAddress($country,$city,$province,$specificAddress){
    return "$country, $city, $province, $specificAddress ";
};

function getLetterGrade($grade){

    try{
        if($grade >= 98 ){
            echo $grade;
        } else if($grade >= 95 && $grade <= 97){
            echo $grade;
        } else if($grade >= 92 && $grade <= 94){
            echo $grade;
        } else if($grade >= 89 && $grade <= 91){
            echo $grade;
        } else if($grade >= 86 && $grade <= 88){
            echo $grade;
        } else if($grade >= 83 && $grade <= 85){
            echo $grade;
        } else if($grade >= 80 && $grade <= 82){
            echo $grade;
        } else if($grade >= 77 && $grade <= 79){
            echo $grade;
        } else if($grade >= 75 && $grade <= 76){
            echo $grade;
        } else if ($grade <= 74 && $grade >= 0) {
            echo $grade;
        } else {
            throw new Exception("Outside Parameter");
        }
    } catch(Exception $e) {
        echo $e->getMessage();
    } finally{
        if($grade >= 98 ){
            echo ' is equivalent to A+';
        } else if($grade >= 95 && $grade <= 97){
            echo ' is equivalent to A';
        } else if($grade >= 92 && $grade <= 94){
            echo ' is equivalent to A-';
        } else if($grade >= 89 && $grade <= 91){
            echo ' is equivalent to B+';
        } else if($grade >= 86 && $grade <= 88){
            echo ' is equivalent to B';
        } else if($grade >= 83 && $grade <= 85){
            echo ' is equivalent to B-';
        } else if($grade >= 80 && $grade <= 82){
            echo ' is equivalent to C+';
        } else if($grade >= 77 && $grade <= 79){
            echo ' is equivalent to C';
        } else if($grade >= 75 && $grade <= 76){
            echo ' is equivalent to C-';
        } else if ($grade <= 74 && $grade >= 0) {
            echo ' is equivalent to F';
        } 
    }

}

